# Deploy kafka
use your package manager if you have a distribution worthy of the name

use the jar binanry https://kafka.apache.org/quickstart . Start zookeeper then kafka

use the docker image (lazy way !)
```bash
docker-compose -f docker-compose-single-broker.yml -f ../docker-compose-single-broker.override.yml up
```

# Deploy kafka on k8s

```bash
helm init
helm repo add confluentinc https://raw.githubusercontent.com/confluentinc/cp-helm-charts/master
helm repo update
# helm install confluentinc/cp-helm-charts --name test-kafka #generic
helm install --set cp-schema-registry.enabled=false,cp-kafka-rest.enabled=false,cp-kafka-connect.enabled=false confluentinc/cp-helm-charts

```

with kops
```bash
kubectl create serviceaccount --namespace kube-system tiller
kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'
```
